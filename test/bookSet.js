'use strict'
const expect  = require("chai").expect;
const bookSet = require("../lib/bookSet");
//const ssExcep = require("../lib/SortingServiceException");

const sampleBooks = [
  {
    title:   "Clean Code: A Handbook of Agile Software Craftsmanship",
    author:  "Robert C Martin",
    edition: "2008"
  },
  {
    title:   "The Browser Hacker's Handbook",
    author:  "Wade Alcorn, Christian Frichot, Michele Orru",
    edition: "2014"
  },
  {
    title:   "Learning GraphQL",
    author:  "Eve Porcello e Alex Banks",
    edition: "2018"
  },
  {
    title:   "Steve Jobs",
    author:  "Walter Isaacson",
    edition: "2011"
  }
];

describe("BookSet", () => {

  it("Title ascending", () => {
    const ordered = bookSet.organize(sampleBooks, {field: 'title',order: 'asc'})

    expect(ordered[0].title).to.equal("Clean Code: A Handbook of Agile Software Craftsmanship");
    expect(ordered[1].title).to.equal("Learning GraphQL");
    expect(ordered[2].title).to.equal("Steve Jobs");
    expect(ordered[3].title).to.equal("The Browser Hacker's Handbook");
  });

  it("Author ascending, Title descending", () => {
    const ordered = bookSet.organize(sampleBooks, {field: 'author',order: 'asc'}, {field: 'title',order: 'desc'})

    expect(ordered[0].title).to.equal("Learning GraphQL");
    expect(ordered[1].title).to.equal("Clean Code: A Handbook of Agile Software Craftsmanship");
    expect(ordered[2].title).to.equal("The Browser Hacker's Handbook");
    expect(ordered[3].title).to.equal("Steve Jobs");
  })

  it("Edition descending, Author descending, Title ascending", () => {
    const ordered = bookSet.organize(sampleBooks, {field: 'edition', order: 'desc'}, {field: 'author',order: 'desc'}, {field: 'title', order: 'asc'})

    expect(ordered[0].title).to.equal("Learning GraphQL");
    expect(ordered[1].title).to.equal("The Browser Hacker's Handbook");
    expect(ordered[2].title).to.equal("Steve Jobs");
    expect(ordered[3].title).to.equal("Clean Code: A Handbook of Agile Software Craftsmanship");
  })
});

describe("BookSet - FAILS", () => {

  it("Empty filter forcing null", done => {
    done(); //TODO remove
    //expect(bookSet.organize(sampleBooks, null)).to.throw(new ssExcep('Null parameter')); //TODO fix
  });

  it("Empty filter", () => {
    const ordered = bookSet.organize(sampleBooks)

    expect(ordered).to.be.an('array');
    expect(ordered).to.be.empty;
  });
});