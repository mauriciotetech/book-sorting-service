'use strict'

exports.responseCallback = (statusCode, body) => {
    return {
        statusCode: statusCode,
        headers: {
            "Cache-Control": "max-age=0",
            "Content-type" : "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(body)
    };
};