class SortingServiceException extends Error {
    constructor(message) {
        super(message);
        this.name = 'SortingServiceException';
    }
}

module.exports = SortingServiceException;