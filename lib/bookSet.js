'use strict'
const _           = require('lodash');
const ssException = require('./SortingServiceException')
const isValidField = f => f === 'author' || f === 'edition' || f === 'title';

const isValidOrder = o => o === 'asc' || o === 'desc';

exports.organize = (bookSet, ...filters) => {
    if(filters.length === 0 || ! bookSet) return [];
    let fields = [], orders = [];

    filters.map(a => {
        if(a === null) throw new ssException('Null parameter');
        if(!isValidField(a.field) || !isValidOrder(a.order)) 
            throw new ssException('Invalid parameter');
        fields.push(a.field);
        orders.push(a.order);
    });

    return _.orderBy(bookSet, fields, orders);
};