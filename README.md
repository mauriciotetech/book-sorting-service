# E-Commerce Books Sorting Service

* [Guidelines](#guidelines)
* [Architecture](#Architecture)
* [Tecnologies](#Tecnologies)
* [Design](#Design)
* [CI/CD](#CI/CD)
* [Running locally](#Running locally)
* [Requirements](#Requirements)
* [How to run](#How to run)
* [Local API calls](#Local API calls)
* [API USE](#API USE)

## Guidelines

This document provides guidelines and examples on how to consume E-Commerce Sorting Service API.

## Architecture

According to the documentation provided, was chosen the nano services architecture to provide high scalability, stateless behavior, loose coupling and costs reduction.

## Tecnologies

AWS Services are used in development and production envoirement. Aws Lambda provides nano services architecture and AWS API Gateway provides public API endpoints to be consumed. Docker is also used to build a local development envoirement and during the CI/CD process created on Bitbucket.

* Aws Lambda
    * https://docs.aws.amazon.com/lambda/latest/dg/welcome.html
* API Gateway
    * https://docs.aws.amazon.com/apigateway/latest/developerguide/welcome.html
* Serverless Framework - Used to help the development and deployment.
     * https://serverless.com/framework/docs/providers/aws/guide/
* Docker.
     * https://docs.docker.com/

## Design

The sorting service was created on a separeted module that can be used in any place of the application without changing the code behavior. To able the users to consume the service a REST API endpoint was created requiring the module.

```
|-- functions
|   `-- sortBooksFunc.js
|-- lib
|   |-- bookSet.js
|   |-- SortingServiceException.js
|   `-- util.js
|-- test
|   `-- bookSet.js
```

#CI/CD

To ensure quality and help the resources deployment a Bitbucket Pipeline was created. The master branch is locked for commits, so first the dev branch recieves an commit, starts the pipeline and then if the pipeline result is ok a pull request is created to master branch and the pipeline is triggered in production.


## Running locally

### Requirements

Docker is required to execute the local nano services envoirement and npm to install and run the packages.

* Docker.
     * https://docs.docker.com/
* Npm.
     * https://docs.docker.com/

### How to run

First the dependencies need to be installed and then the API can be executed locally.
```
npm install
npm run local
```

The scripts will install the dependencies and run a local docker container. 

### Local API calls

**Sample OK requisition:**

- **POST** [http://localhost:3000/api/bookSorting/v1/sort?edition=desc](http://localhost:3000/api/bookSorting/v1/sort?edition=desc)
- **Content-Type:** application/json

```html
	{
        "bookSet": [
            {
                "title":   "Clean Code: A Handbook of Agile Software Craftsmanship",
                "author":  "Robert C Martin",
                "edition": "2008"
            },
            {
                "title":   "The Browser Hacker's Handbook",
                "author":  "Wade Alcorn, Christian Frichot, Michele Orru",
                "edition": "2014"
            }
        ]
    }
```

**Sample response:**

- **200** OK
- **Content-Type:** application/json
- **Cache-Control:** no-cache
```html
	{
        "bookSet": [
            {
                "title": "The Browser Hacker's Handbook",
                "author": "Wade Alcorn, Christian Frichot, Michele Orru",
                "edition": "2014"
            },
            {
                "title": "Clean Code: A Handbook of Agile Software Craftsmanship",
                "author": "Robert C Martin",
                "edition": "2008"
            }
        ]
    }
```

**Sample ERROR requisitions:**

- **POST** [http://localhost:3000/api/bookSorting/v1/sort?err=desc](http://localhost:3000/api/bookSorting/v1/sort?err=desc)
- **Content-Type:** application/json

```html
	{
        "bookSet": [
            {
                "title":   "Clean Code: A Handbook of Agile Software Craftsmanship",
                "author":  "Robert C Martin",
                "edition": "2008"
            },
            {
                "title":   "The Browser Hacker's Handbook",
                "author":  "Wade Alcorn, Christian Frichot, Michele Orru",
                "edition": "2014"
            }
        ]
    }
```

**Sample response:**

- **400** BadRequest
- **Content-Type:** application/json
- **Cache-Control:** no-cache
```html
	{
        "err": "SortingServiceException",
        "message": "Invalid parameter"
    }
```

- **POST** [http://localhost:3000/api/bookSorting/v1/sort?edition=desc](http://localhost:3000/api/bookSorting/v1/sort?edition=desc)
- **Content-Type:** application/json

```html
	{
        "bookSet": [
            {
                "title":   "Clean Code: A Handbook of Agile Software Craftsmanship",
                "author":  "Robert C Martin",
                "edition": "2008"
            },
            {
                "title":   "The Browser Hacker's Handbook",
                "author":  "Wade Alcorn, Christian Frichot, Michele Orru",
                "edition": "2014"
            }
        ]
    }
```

**Sample response:**

- **400** BadRequest
- **Content-Type:** application/json
- **Cache-Control:** no-cache
```html
	{
        "err": "SyntaxError",
        "message": "Invalid bookSet"
    }
```

## API USE

The api can be called with POST requisitios as descibre on samples given on the [Local API calls](#Local API calls) section, two envoirements are able now to ble called on AWS following the instructions given:

* AWS dev env endpoint
    * POST - https://zj9ds97t8h.execute-api.sa-east-1.amazonaws.com/dev/api/bookSorting/v1/sort
* AWS prod env
    * POST - https://u1smddsxp7.execute-api.sa-east-1.amazonaws.com/prod/api/bookSorting/v1/sort