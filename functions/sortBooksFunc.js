'use strict';
const util = require('../lib/util');
const ss   = require('../lib/bookSet');

module.exports.handler = (event, context, callback) => {
    try{
        const books = JSON.parse(event.body).bookSet;
        let filters = [];
        const params = event.queryStringParameters;

        for(let key in params) filters.push({field: key, order: params[key]});

        return callback(null, util.responseCallback(200, {bookSet: ss.organize(books, ...filters)}));
    }catch (e){
        console.log(e);
        let msg, code;
        switch(e.name){
            case 'SyntaxError':
                code = 400;
                msg = "Invalid bookSet";
                break;
            case 'SortingServiceException':
                code = 400;
                msg = e.message;
                break;
            default:
                code = 500;
                msg = "Internal error";
                break;
        }
        return callback(null, util.responseCallback(400, {err: e.name, message: msg}));
    }
}